/*
 ******************************************************************************
 * @file    GR.h
 * @author  Sofiane AOUCI
 * @brief   Gesture Recognition Application header
 ******************************************************************************
*/
#ifndef INC_GR_H_
#define INC_GR_H_

#include "main.h"
#include "lsm9ds1.h"
#include "app_x-cube-ai.h"
#include "usart.h"


#define LSM9DS1_FS LSM9DS1_2g
#define max_data_length 4000
#define ts 1e-3

void GR_Init();
void GR_Calibration();
void GR_data_acquisition();
void GR_data_processing();
void GR_image_generation();
void GR_upload_image();
uint8_t GR_DR();

#endif /* INC_GR_H_ */
