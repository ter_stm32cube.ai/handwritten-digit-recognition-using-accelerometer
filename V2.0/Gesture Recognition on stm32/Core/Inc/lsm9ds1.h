/*
 ******************************************************************************
 * @file    lsm9ds1_reg.h
 * @author  Sofiane AOUCI
 * @brief   This file contains all the functions prototypes for the
 *          lsm9ds1.c driver.
 ******************************************************************************
*/

#ifndef LSM9DS1_H
#define LSM9DS1_H

#include <stdint.h>
#include <math.h>


#define LSM9DS1_IMU_I2C_ADD		0xD7U
#define LSM9DS1_IMU_ID          0x68U

#define LSM9DS1_OUT_X_L         0x28U
#define LSM9DS1_OUT_X_H         0x29U
#define LSM9DS1_OUT_Y_L         0x2AU
#define LSM9DS1_OUT_Y_H         0x2BU
#define LSM9DS1_OUT_Z_L         0x2CU
#define LSM9DS1_OUT_Z_H         0x2DU

#define LSM9DS1_CTRL_REG5       0x1FU
#define LSM9DS1_CTRL_REG6       0x20U
#define LSM9DS1_CTRL_REG7       0x21U
#define LSM9DS1_STATUS_REG      0x17U

#define  LSM9DS1_2g     		0.061f
#define  LSM9DS1_16g    		0.122f
#define  LSM9DS1_4g     		0.244f
#define  LSM9DS1_8g     		0.732f

typedef struct {
  uint8_t 			               : 3;
  uint8_t xen_xl                   : 1;
  uint8_t yen_xl                   : 1;
  uint8_t zen_xl                   : 1;
  uint8_t dec                      : 2;
} lsm9ds1_ctrl_reg5_t;

typedef struct {
  uint8_t bw_xl                    : 2;
  uint8_t bw_scal_odr              : 1;
  uint8_t fs_xl                    : 2;
  uint8_t odr_xl                   : 3;
} lsm9ds1_ctrl_reg6_t;

typedef struct {
  uint8_t hpis1                    : 1;
  uint8_t 			               : 1;
  uint8_t fds                      : 1;
  uint8_t 			               : 2;
  uint8_t dcf                      : 2;
  uint8_t hr                       : 1;
} lsm9ds1_ctrl_reg7_t;

typedef struct {
  uint8_t xlda                     : 1;
  uint8_t gda                      : 1;
  uint8_t tda                      : 1;
  uint8_t boot_status              : 1;
  uint8_t inact                    : 1;
  uint8_t ig_g                     : 1;
  uint8_t ig_xl                    : 1;
  uint8_t 			               : 1;
} lsm9ds1_status_reg_t;

typedef struct {
	int16_t X;
	int16_t Y;
	int16_t Z;
} lsm9ds1_accel_t;

typedef struct {
	float_t X;
	float_t Y;
	float_t Z;
} lsm9ds1_accel_mg_t;


void lsm9ds1_Init();
lsm9ds1_accel_t lsm9ds1_readData();
lsm9ds1_accel_mg_t lsm9ds1_from_fsg_to_mg(float_t fs, lsm9ds1_accel_t accel);

#endif
