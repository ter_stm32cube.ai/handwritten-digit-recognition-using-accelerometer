/*
 ******************************************************************************
 * @file    GR.c
 * @author  Sofiane AOUCI
 * @brief   Gesture Recognition Application algorithms
 ******************************************************************************
*/

#include "GR.h"
#include <math.h>

#define max(a,b) a>b?a:b

typedef lsm9ds1_accel_mg_t coord_t;

/*********************************************************************************
 *                              External variables				    			 *
 *********************************************************************************/
extern ai_float* in;


/*********************************************************************************
 * 								Private Variables								 *
 *********************************************************************************/
ai_float image_t[28][28] = {0};

coord_t accel_data[max_data_length];
coord_t s_data[max_data_length];
coord_t pos_data[max_data_length];
coord_t max_pos, min_pos;
coord_t accel_offset;
uint16_t data_length = 0;


/*********************************************************************************
 *						  Private functions prototypes							 *
 *********************************************************************************/
inline void setValue(coord_t* c, float X, float Y, float Z);
inline void equation(coord_t *a, coord_t b, coord_t c, coord_t d);
inline void reset_data();


/*********************************************************************************
 * 						  Private functions definition							 *
 *********************************************************************************/

/**
 * assigns X Y Z to the corresponding fields of c
 */
void setValue(coord_t* c, float X, float Y, float Z){
	c->X = X;
	c->Y = Y;
	c->Z = Z;
}

/**
 * It's the implementation of the equation : a = b + c * d
 * It's a way to do additions/multiplications with the coord_t struct
 */
void equation(coord_t *a, coord_t b, coord_t c, coord_t d){
	setValue(a,
			 b.X + c.X * d.X,
			 b.Y + c.Y * d.Y,
			 b.Z + c.Z * d.Z);
}

/**
 * cleans the image and reset the data length parameter
 */
void reset_data(){
	data_length = 0;
	int i,j;
	for(i = 0; i < 28; i++){
		for(j = 0; j < 28; j++) image_t[i][j] = 0;
	}
}


/*********************************************************************************
 * 								   Algorithms									 *
 *********************************************************************************/

/**
 * Calibrates the accelerometer
 */
void GR_Calibration()
{
	while (data_length < 1000)
	{
		accel_data[data_length++] = lsm9ds1_from_fsg_to_mg(LSM9DS1_FS, lsm9ds1_readData());
		HAL_Delay(2);
	}

	int i;
	accel_offset.X = 0;
	accel_offset.Y = 0;
	accel_offset.Z = 0;
	for(i = 0; i < data_length; i++){
		accel_offset.X += accel_data[i].X;
		accel_offset.Y += accel_data[i].Y;
		accel_offset.Z += accel_data[i].Z;
	}
	accel_offset.X /= data_length;
	accel_offset.Y /= data_length;
	accel_offset.Z /= data_length;

	data_length = 0;
}

/**
 * Lecture of the accelerometer sensor
 */
void GR_data_acquisition()
{
	accel_data[data_length++] = lsm9ds1_from_fsg_to_mg(LSM9DS1_FS, lsm9ds1_readData());
}

/**
 * generates the positions vector from the acceleration vector
 */
void GR_data_processing()
{
	setValue(&pos_data[0], 0, 0, 0);
	setValue(&s_data[0], 0, 0, 0);
	setValue(&max_pos, 0, 0, 0);
	setValue(&min_pos, 0, 0, 0);

	int i;

	for(i = 0; i < data_length; i++)
	{
		accel_data[i].X -= accel_offset.X;
		accel_data[i].Y = - accel_data[i].Y + accel_offset.Y;
		accel_data[i].Z -= accel_offset.Z;
	}

	for(i = 0; i < data_length - 1; i++)
	{
		s_data[i+1].X = s_data[i].X + ts * (accel_data[i].X + accel_data[i+1].X) / 2;
		s_data[i+1].Y = s_data[i].Y + ts * (accel_data[i].Y + accel_data[i+1].Y) / 2;
		s_data[i+1].Z = s_data[i].Z + ts * (accel_data[i].Z + accel_data[i+1].Z) / 2;
	}

	for(i = 0; i < data_length - 1; i++)
	{
		pos_data[i+1].X = pos_data[i].X + ts * (s_data[i].X + s_data[i+1].X) / 2;
		pos_data[i+1].Y = pos_data[i].Y + ts * (s_data[i].Y + s_data[i+1].Y) / 2;
		pos_data[i+1].Z = pos_data[i].Z + ts * (s_data[i].Z + s_data[i+1].Z) / 2;

		if( max_pos.X < pos_data[i+1].X)  max_pos.X = pos_data[i+1].X;
		if( max_pos.Y < pos_data[i+1].Y)  max_pos.Y = pos_data[i+1].Y;
		if( max_pos.Z < pos_data[i+1].Z)  max_pos.Z = pos_data[i+1].Z;

		if( min_pos.X > pos_data[i+1].X)  min_pos.X = pos_data[i+1].X;
		if( min_pos.Y > pos_data[i+1].Y)  min_pos.Y = pos_data[i+1].Y;
		if( min_pos.Z > pos_data[i+1].Z)  min_pos.Z = pos_data[i+1].Z;
	}
}


/**
 * Generates a 28x28 pixels image Y = f(X) from the position vector
 */
void GR_image_generation()
{
	coord_t dummy, length, max_s_data, min_s_data;

	setValue(&dummy, -1, -1, -1);
	equation(&length, max_pos, min_pos, dummy);
	setValue(&max_s_data, 0, 0, 0);
	setValue(&min_s_data, 0, 0, 0);

	float max_length = max(length.X,length.Y);

	int i;
	for(i = 0; i < data_length; i++)
	{
		setValue(s_data + i,
				floor((pos_data[i].X + fabs(min_pos.X)) * 15 / max_length) + 1,
				floor((pos_data[i].Y + fabs(min_pos.Y)) * 15 / max_length) + 1,
				0);

		if( max_s_data.X < s_data[i].X)  max_s_data.X = s_data[i].X;
		if( max_s_data.Y < s_data[i].Y)  max_s_data.Y = s_data[i].Y;

		if( min_s_data.X > s_data[i].X)  min_s_data.X = s_data[i].X;
		if( min_s_data.Y > s_data[i].Y)  min_s_data.Y = s_data[i].Y;
	}

	for(i = 0; i < data_length; i++)
	{
		setValue(s_data + i,
				 s_data[i].X + floor((28 - (max_s_data.X - min_s_data.X + 1))/2),
				 s_data[i].Y + floor((28 - (max_s_data.Y - min_s_data.Y + 1))/2),
				 0);
	}

	for(i = 0; i < data_length; i++)
	{
		if(s_data[i].X > 0 && s_data[i].Y > 0)
		{
			image_t[(int)(s_data[i].X)][(int)(s_data[i].Y)] = 255;
			image_t[(int)(s_data[i].X)][(int)(s_data[i].Y+1)] = 255;
			image_t[(int)(s_data[i].X+1)][(int)(s_data[i].Y)] = 255;
		}

	}

}

/**
 * sends the image over USART
 */
void GR_upload_image()
{
	int i,j;
	for(i = 0; i < 28; i++)
		for(j = 0; j < 28; j++){
			HAL_UART_Transmit(&huart3, (uint8_t*)&(image_t[i][j]), sizeof(image_t[i][j]), 1000);
		}
}


/**
 * runs the neural network with the image as input and returns the result
 * cleans the data for the next gesture recognition
 */
uint8_t GR_DR()
{
	in = (ai_float*) image_t;
	uint8_t result = MX_X_CUBE_AI_Process();
	reset_data();
	return result;
}

