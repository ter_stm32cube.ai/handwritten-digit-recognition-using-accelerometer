# HANDWRITTEN DIGITS RECOGNITION USING ACCELEROMETER

In this project, we use an accelerometer to draw digits and try to recognize them using a neural network on an stm32.

## Hardware
*  Nucleo F429ZI
*  ST LSM9DS1 IMU

## Software
*  STM32CubeIDE
*  STM32Cube.AI Framwork
*  Matlab

## Project Versions

### V1.0
It's a test project, here we use Matlab for the data processing and visualisation to validate the concept :
*  The neural network is implemented on the stm32
*  The accelerometer data are read by the stm32 and sent over UART to matlab using a serial Port
*  The data are processed in matlab :
    *  Numerical integration to get the position
    *  28x28 pixels image generation using the position
*  The image is sent to the stm32 and feed to the CNN
*  The prediction is sent to matlab and displayed on the console

### V2.0
In this version, all the algorithms are implemented on the stm32 :
*  The accelerometer data aquisition and processing is done on the stm32
*  An image is generated and feed to the CNN
*  The image and the prediction is sent over the serial port to be displayed

##### Here is a [DEMO](https://youtu.be/x09KEeED0wA)