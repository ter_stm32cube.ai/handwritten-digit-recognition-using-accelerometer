/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_x-cube-ai.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lsm9ds1_reg.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define G_OFF
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart3;

PCD_HandleTypeDef hpcd_USB_OTG_FS;

/* USER CODE BEGIN PV */
volatile uint8_t uartRx[28][28];
ai_float _in[28][28];
ai_float* in = NULL;
//int i = 0;
uint8_t ok[] = "OK";


static float acceleration_mg[3];
static float geyroscope_dps[3];
static lsm9ds1_id_t whoamI;
static lsm9ds1_status_t reg;

stmdev_ctx_t dev_ctx_mag;
stmdev_ctx_t dev_ctx_imu;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CRC_Init(void);
static void MX_TIM3_Init(void);
void MX_USART3_UART_Init(void);
static void MX_USB_OTG_FS_PCD_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */
static int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
void lsm9ds1_Init(void);
void lsm9ds1_readData();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CRC_Init();
  MX_TIM3_Init();
  MX_USB_OTG_FS_PCD_Init();
  MX_I2C1_Init();
  MX_X_CUBE_AI_Init();
  /* USER CODE BEGIN 2 */
  MX_USART3_UART_Init();
//  HAL_TIM_Base_Start_IT(&htim3);
  //i = 0;
  lsm9ds1_Init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

 // MX_X_CUBE_AI_Process();
    /* USER CODE BEGIN 3 */
  if(HAL_GPIO_ReadPin(USER_Btn_GPIO_Port, USER_Btn_Pin))
  {
	  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, 0);

	  HAL_Delay(5);
	  HAL_TIM_Base_Start_IT(&htim3);
	  while(HAL_GPIO_ReadPin(USER_Btn_GPIO_Port, USER_Btn_Pin)) /*lsm9ds1_readData()*/;
	  HAL_TIM_Base_Stop_IT(&htim3);

  }
  else {
	  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, 1);
	  HAL_UART_Receive_IT(&huart3, uartRx, 28*28*sizeof(uint8_t));

  }


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 16800-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 10-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 921600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief USB_OTG_FS Initialization Function
  * @param None
  * @retval None
  */
static void MX_USB_OTG_FS_PCD_Init(void)
{

  /* USER CODE BEGIN USB_OTG_FS_Init 0 */

  /* USER CODE END USB_OTG_FS_Init 0 */

  /* USER CODE BEGIN USB_OTG_FS_Init 1 */

  /* USER CODE END USB_OTG_FS_Init 1 */
  hpcd_USB_OTG_FS.Instance = USB_OTG_FS;
  hpcd_USB_OTG_FS.Init.dev_endpoints = 4;
  hpcd_USB_OTG_FS.Init.speed = PCD_SPEED_FULL;
  hpcd_USB_OTG_FS.Init.dma_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
  hpcd_USB_OTG_FS.Init.Sof_enable = ENABLE;
  hpcd_USB_OTG_FS.Init.low_power_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.lpm_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.vbus_sensing_enable = ENABLE;
  hpcd_USB_OTG_FS.Init.use_dedicated_ep1 = DISABLE;
  if (HAL_PCD_Init(&hpcd_USB_OTG_FS) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USB_OTG_FS_Init 2 */

  /* USER CODE END USB_OTG_FS_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD1_Pin|LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : USER_Btn_Pin */
  GPIO_InitStruct.Pin = USER_Btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_Btn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_MDC_Pin RMII_RXD0_Pin RMII_RXD1_Pin */
  GPIO_InitStruct.Pin = RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_REF_CLK_Pin RMII_MDIO_Pin RMII_CRS_DV_Pin */
  GPIO_InitStruct.Pin = RMII_REF_CLK_Pin|RMII_MDIO_Pin|RMII_CRS_DV_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LD1_Pin LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD1_Pin|LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : RMII_TXD1_Pin */
  GPIO_InitStruct.Pin = RMII_TXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(RMII_TXD1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_TX_EN_Pin RMII_TXD0_Pin */
  GPIO_InitStruct.Pin = RMII_TX_EN_Pin|RMII_TXD0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	//HAL_GPIO_TogglePin(LD1_GPIO_Port,LD1_Pin);
	lsm9ds1_readData();

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin,0);
	int i = 0, j = 0;
	for(i = 0; i < 28; i++)
	{
		for(j = 0; j < 28; j++)
			{
				_in[i][j] = (ai_float) uartRx[i][j];
			}
	}
	in = _in;
	//HAL_UART_Transmit_IT(huart, ok, 3 * sizeof(uint8_t));
	MX_X_CUBE_AI_Process();

	//HAL_UART_Transmit_IT(huart,&uartRx,sizeof(uint8_t));
}




static int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
  uint8_t *i2c_address = handle;
  HAL_I2C_Mem_Write(&hi2c1, *((uint8_t*) i2c_address), reg, I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  return 0;
}

static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len)
{
  uint8_t *i2c_address = handle;
  HAL_I2C_Mem_Read(&hi2c1, *((uint8_t*)i2c_address), reg, I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  return 0;
}

void lsm9ds1_Init()
{


	/* Initialize magnetic sensors driver interface */
	  uint8_t i2c_add_mag = LSM9DS1_MAG_I2C_ADD_L;
	  dev_ctx_mag.write_reg = platform_write;
	  dev_ctx_mag.read_reg = platform_read;
	  dev_ctx_mag.handle = (void*)&i2c_add_mag;

	  /* Initialize inertial sensors (IMU) driver interface */
	  uint8_t i2c_add_imu = LSM9DS1_IMU_I2C_ADD_H;
	  dev_ctx_imu.write_reg = platform_write;
	  dev_ctx_imu.read_reg = platform_read;
	  dev_ctx_imu.handle = (void*)&i2c_add_imu;

	  HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_WHO_AM_I, I2C_MEMADD_SIZE_8BIT, &(whoamI.imu), sizeof(whoamI.imu), 1000);
	  HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_MAG_I2C_ADD_L, LSM9DS1_WHO_AM_I_M, I2C_MEMADD_SIZE_8BIT, &(whoamI.mag), sizeof(whoamI.mag), 1000);

	  /* Check device ID */
	  lsm9ds1_dev_id_get(&dev_ctx_mag, &dev_ctx_imu, &whoamI);
	  if (whoamI.imu != LSM9DS1_IMU_ID || whoamI.mag != LSM9DS1_MAG_ID){
		  HAL_GPIO_WritePin(LD3_GPIO_Port,LD1_Pin,1);
		  while(1){
	      /* manage here device not found */
	    }
	  }
	  //HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,1);

	  lsm9ds1_ctrl_reg6_xl_t ctrl_reg6_xl;
	  ctrl_reg6_xl.odr_xl = 6;
	  ctrl_reg6_xl.fs_xl = 0;
	  ctrl_reg6_xl.bw_scal_odr = 1;
	  ctrl_reg6_xl.bw_xl = 3;
	  HAL_I2C_Mem_Write(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_CTRL_REG6_XL, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &ctrl_reg6_xl, sizeof(ctrl_reg6_xl), 1000);


	  lsm9ds1_ctrl_reg7_xl_t ctrl_reg7_xl;
	  ctrl_reg7_xl.hr = 1;
	  ctrl_reg7_xl.dcf = 0;
	  ctrl_reg7_xl.fds = 0;
	  ctrl_reg7_xl.hpis1 = 0;
	  HAL_I2C_Mem_Write(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_CTRL_REG7_XL, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &ctrl_reg7_xl, sizeof(ctrl_reg7_xl), 1000);

	  uint8_t ctrl_reg5_xl = 0b00111000; //default value 0b00111000
	  HAL_I2C_Mem_Write(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_CTRL_REG5_XL, I2C_MEMADD_SIZE_8BIT, &ctrl_reg5_xl, sizeof(ctrl_reg5_xl), 1000);


#ifndef G_OFF
	  lsm9ds1_ctrl_reg1_g_t ctrl_reg1_G;
	  ctrl_reg1_G.odr_g = 6;
	  ctrl_reg1_G.fs_g = 0;
	  ctrl_reg1_G.bw_g = 0;
	  HAL_I2C_Mem_Write(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_CTRL_REG1_G, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &ctrl_reg1_G, sizeof(ctrl_reg1_G), 1000);

#endif

	 /* lsm9ds1_xl_axis_t Ax_t;
	  Ax_t.xen_xl = 1;
	  Ax_t.yen_xl = 1;
	  Ax_t.zen_xl = 1;

	  lsm9ds1_imu_data_rate_set(&dev_ctx_imu, LSM9DS1_GY_OFF_XL_952Hz);
	  lsm9ds1_xl_full_scale_set(&dev_ctx_imu, LSM9DS1_4g);
	  lsm9ds1_xl_filter_aalias_bandwidth_set(&dev_ctx_imu, LSM9DS1_408Hz);
	  lsm9ds1_xl_axis_set(&dev_ctx_imu, Ax_t);
	  lsm9ds1_xl_decimation_set(&dev_ctx_imu, LSM9DS1_NO_DECIMATION);*/

}

void lsm9ds1_readData()
{
	lsm9ds1_dev_status_get(&dev_ctx_mag, &dev_ctx_imu, &reg);
	HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_STATUS_REG, I2C_MEMADD_SIZE_8BIT, (uint8_t*)&(reg.status_imu), sizeof(reg.status_imu), 1000);

#ifdef G_OFF
	uint8_t data[6];
	uint16_t accel[3];


	    if ( reg.status_imu.xlda )
	    {
	    	//lsm9ds1_read_reg(&dev_ctx_imu,LSM9DS1_OUT_X_L_XL, data, 6 * sizeof(uint8_t));
	    	HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_OUT_X_L_XL, I2C_MEMADD_SIZE_8BIT, data, 6 * sizeof(uint8_t), 1000);
	    	accel[0] = ((data[1] << 8) | data[0]);
	    	accel[1] = ((data[3] << 8) | data[2]);
	    	accel[2] = ((data[5] << 8) | data[4]);

	    	acceleration_mg[0] = lsm9ds1_from_fs2g_to_mg(accel[0]);
	    	acceleration_mg[1] = lsm9ds1_from_fs2g_to_mg(accel[1]);
	    	acceleration_mg[2] = lsm9ds1_from_fs2g_to_mg(accel[2]);


/*
	      sprintf((char*)tx_buffer, "X = %f       Y = %f       Z = %f\r\n",
	    		  acceleration_mg[0], acceleration_mg[1], acceleration_mg[2]);*/
	      HAL_UART_Transmit(&huart3, (uint8_t *) accel, 3 * sizeof(uint16_t), 1000);
	    }
#else
	    uint8_t data[12];
	    uint16_t accel[3];
	    uint16_t geyro[3];


	    if ( reg.status_imu.xlda && reg.status_imu.xlda)
	    {
	    	//lsm9ds1_read_reg(&dev_ctx_imu,LSM9DS1_OUT_X_L_XL, data, 6 * sizeof(uint8_t));
	    	HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_IMU_I2C_ADD_H, LSM9DS1_OUT_X_L_G, I2C_MEMADD_SIZE_8BIT, data, 12 * sizeof(uint8_t), 5000);
	    	geyro[0] = ((data[1] << 8) | data[0]);
	    	geyro[1] = ((data[3] << 8) | data[2]);
	    	geyro[2] = ((data[5] << 8) | data[4]);
	    	accel[0] = ((data[7] << 8) | data[6]);
	    	accel[1] = ((data[9] << 8) | data[8]);
	    	accel[2] = ((data[11] << 8) | data[10]);

	    	acceleration_mg[0] = lsm9ds1_from_fs2g_to_mg(accel[0]);
	    	acceleration_mg[1] = lsm9ds1_from_fs2g_to_mg(accel[1]);
	    	acceleration_mg[2] = lsm9ds1_from_fs2g_to_mg(accel[2]);
	    	geyroscope_dps[0] = lsm9ds1_from_fs245dps_to_mdps(geyro[0]);
	    	geyroscope_dps[1] = lsm9ds1_from_fs245dps_to_mdps(geyro[1]);
	    	geyroscope_dps[2] = lsm9ds1_from_fs245dps_to_mdps(geyro[2]);
	    	HAL_UART_Transmit(&huart3, (uint8_t *) data, 12 * sizeof(uint8_t), 1000);
	    	//HAL_UART_Transmit(&huart3, (uint8_t *) geyro, 3 * sizeof(uint16_t), 1000);
	    }

#endif
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
