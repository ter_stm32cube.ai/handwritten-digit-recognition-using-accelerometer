% File : Image Generation
% Author : Sofiane AOUCI
% Desciption : Generate a 28x28 image

clc
p = 17;
% t_image = zeros(p+1);
X_length = max(X(:,1)) - min(X(:,1));
Y_length = max(X(:,2)) - min(X(:,2));

t_X = X(:,1:2) + abs(min(X(:,1:2)));
t_X = floor(t_X * p ./ max([X_length, Y_length])) + 1;

t_image = zeros(28);
t_X = t_X + floor( (28 - (max(t_X) - min(t_X) + 1))/2);
for i = 1:length(t_X)
    if (t_X(i,1) ~= 0) && (t_X(i,2) ~= 0)
        t_image(t_X(i,1),t_X(i,2)) = 255;
    end
end
% t_image = [zeros((28 - p - 1)/2,p+1); t_image; zeros((28 - p - 1)/2,p+1)];
% t_image = [zeros(28,(25-p)/2 + 1), t_image, zeros(28,(25-p)/2 + 1)];
close all
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [Imx Imy] = find(t_image == 255);
% void = (28 - (max(Imy) - min(Imy) + 1))/2;
% t_image = [zeros(28,floor(void)) t_image(:,min(Imy):max(Imy)) zeros(28,ceil(void))];
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
image(t_image)