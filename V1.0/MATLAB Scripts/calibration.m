% File : Calibration
% Author : Sofiane AOUCI
% Desciption : Calibration of the accelerometer

clear 
close all
clc

fscale = 2;
ts = 0.001;
spl = 3;
t = 0:ts:spl - ts;
n = length(t);
    
s = serialport('COM7',921600)
tic
while s.NumBytesAvailable < n*3
end

a = zeros(n,3);
for in = 1:n
a(in,:) = read(s,3,'int16');
end
toc
clear s

%at = a * fscale*2 / 2^16;
at = a * 0.061;
m = mean(at);
ac = at - m;

figure(1)
plot(t,ac)



