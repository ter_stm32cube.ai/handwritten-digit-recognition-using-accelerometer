% File : Image Upload
% Author : Sofiane AOUCI
% Desciption : Upload image to the STM32

s = serialport('COM7',921600)
disp('PORT opend')
disp('Sending data ...')
img = mod((t_image + [zeros(28,1) t_image(:,1:end-1)] + [zeros(28,1)'; t_image(1:end-1,:)])',256);
write(s,img(:),'uint8');
% write(s,t_image(:)','uint8');
read(s,1,'uint8')
clear s
image(img')
% image(t_image)