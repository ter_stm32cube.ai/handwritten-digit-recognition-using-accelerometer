% File : Data Processing
% Author : Sofiane AOUCI
% Desciption : Calculating position for the data

g = 9.81;

%accel = data(1:(splp/ts),:) - mean(a);
accel = ac;
n = i-1;  
T = t(1:n+1);
V = zeros(n,3);
X = zeros(n,3);

for i = 1:n
    V(i+1,:) = V(i,:) + ts * (accel(i,:) + accel(i+1,:))/2;
end

for i = 1:n
    X(i+1,:) = X(i,:) + ts * (V(i,:) + V(i+1,:))/2;
end

figure(2)

subplot(311)
plot(T,X(:,1))

subplot(312)
plot(T,X(:,2)) 

subplot(313)
plot(T,X(:,3))

figure(3)
plot(X(:,1),X(:,2))