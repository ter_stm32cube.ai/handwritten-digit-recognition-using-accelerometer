% File : Data Acquisition
% Author : Sofiane AOUCI
% Desciption : Accelerometer data acquisition

close all
clc

spl = 5;
t = 0:ts:spl - ts;
n = length(t);

s = serialport('COM7',921600)
disp('PORT opend')
disp('waiting for data...')
tic
    while s.NumBytesAvailable == 0
    end
toc

disp('reading data...')
data = zeros(n,3);
i = 1;
ndata = 0;
while s.NumBytesAvailable ~= ndata
    ndata = s.NumBytesAvailable;
    pause(.1);
end

while s.NumBytesAvailable > 0
    data(i,:) = read(s,3,'int16');
    i = i+1;
end

clear s

disp('processing data...')
%at = data * fscale*2 / 2^16;
at = data * .061;
ac = at - m; %- [0 0 1000];
ac = ac .* [1 -1 1];
% plot(t,ac)
% % 
subplot(311)
plot(t,ac(:,1))

subplot(312)
plot(t,ac(:,2))

subplot(313)
plot(t,ac(:,3))